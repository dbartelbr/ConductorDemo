package db.conductordemo.TodoList;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;
import com.bluelinelabs.conductor.RouterTransaction;
import com.bluelinelabs.conductor.changehandler.HorizontalChangeHandler;
import com.bluelinelabs.conductor.changehandler.VerticalChangeHandler;

import java.util.ArrayList;
import java.util.List;

import db.conductordemo.AddTodo.AddTodoController;
import db.conductordemo.R;
import db.conductordemo.TodoDetail.TodoDetailController;
import db.conductordemo.handler.TodoListHandler;
import db.conductordemo.model.TodoItem;

/**
 * Created by daniel.bartel on 6/3/16.
 */
public class TodoListController extends Controller implements TodoListPresenter.Callbacks {

    List<TodoItem> mTodoItems;
    private static final String TAG = TodoListController.class.getName();
    TodoListPresenter.ViewHolder mViewHolder;
    TodoListHandler mHandler;

    public TodoListController() {
        if (mTodoItems == null) mTodoItems = new ArrayList<>();
    }

    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        try {
            mHandler = (TodoListHandler) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("TodoListController must attach to an activity which implements TodoListHandler");
        }

        mTodoItems = mHandler.getTodoList();

        View view = inflater.inflate(R.layout.view_todo_list, container, false);

        mViewHolder = new TodoListPresenter.ViewHolder(view);
        TodoListPresenter.present(getActivity(), mViewHolder, this, mTodoItems);

        return view;
    }


    @Override
    public void onAddTodo() {
        Log.d(TAG, "onAddTodo");
        getRouter().pushController(RouterTransaction.builder(new AddTodoController())
                .pushChangeHandler(new VerticalChangeHandler())
                .popChangeHandler(new VerticalChangeHandler())
                .build());
    }

    @Override
    public void onCompleteTodo(TodoItem item) {
        Log.d(TAG, "onCompleteTodo");
        mHandler.onRemoveTodo(item);
        mViewHolder.mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onTodoClicked(TodoItem item) {
        Log.d(TAG, "onTodoClicked");
        getRouter().pushController(RouterTransaction.builder(new TodoDetailController(item))
                .pushChangeHandler(new HorizontalChangeHandler())
                .popChangeHandler(new HorizontalChangeHandler())
                .build());
    }
}
