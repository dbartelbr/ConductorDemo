package db.conductordemo.TodoList;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import db.conductordemo.R;
import db.conductordemo.model.TodoItem;

/**
 * Created by daniel.bartel on 6/3/16.
 */
public class TodoListPresenter {

    public interface Callbacks {
        void onAddTodo();

        void onCompleteTodo(TodoItem item);

        void onTodoClicked(TodoItem item);
    }

    public static class ViewHolder {
        RecyclerView mRecyclerView;

        public ViewHolder(View view) {
            mRecyclerView = (RecyclerView) view.findViewById(R.id.todo_recycler);
        }

    }

    public static void present(Context context, ViewHolder viewHolder, Callbacks callbacks, List<TodoItem> items) {
        TodoListAdapter adapter = new TodoListAdapter(items, callbacks);
        viewHolder.mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        viewHolder.mRecyclerView.setAdapter(adapter);

    }

    public static class TodoListAdapter extends RecyclerView.Adapter {
        List<TodoItem> mTodoItems;
        private Callbacks mCallbacks;
        private static final int FOOTER_COUNT = 1;
        private static final int VIEW_TYPE_ITEM = 0;
        private static final int VIEW_TYPE_FOOTER = 1;

        public TodoListAdapter(List<TodoItem> items, Callbacks callbacks) {
            mTodoItems = items;
            mCallbacks = callbacks;
        }

        @Override
        public int getItemViewType(int position) {
            if (position < getItemCount() - FOOTER_COUNT) {
                return VIEW_TYPE_ITEM;
            }
            return VIEW_TYPE_FOOTER;
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View root;
            switch (viewType) {
                case VIEW_TYPE_ITEM:
                    root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_todo, parent, false);
                    return new TodoItemViewHolder(root);
                case VIEW_TYPE_FOOTER:
                    root = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_todo, parent, false);
                    return new AddTodoViewHolder(root);

            }
            return null;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            switch (getItemViewType(position)) {
                case VIEW_TYPE_ITEM:
                    TodoItemViewHolder viewHolder = (TodoItemViewHolder) holder;
                    final TodoItem item = getItem(position);
                    viewHolder.mTitle.setText(item.getTitle());
                    viewHolder.mRoot.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallbacks.onTodoClicked(item);
                        }
                    });

                    viewHolder.mDoneButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallbacks.onCompleteTodo(item);
                        }
                    });

                    break;

                case VIEW_TYPE_FOOTER:
                    AddTodoViewHolder addViewHolder = (AddTodoViewHolder) holder;
                    addViewHolder.mButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mCallbacks.onAddTodo();
                        }
                    });
                    break;
            }
        }

        public TodoItem getItem(int position) {
            return mTodoItems.get(position);
        }

        @Override
        public int getItemCount() {
            return mTodoItems.size() + FOOTER_COUNT;
        }
    }

    public static class TodoItemViewHolder extends RecyclerView.ViewHolder {
        TextView mTitle;
        View mRoot;
        Button mDoneButton;


        public TodoItemViewHolder(View view) {
            super(view);
            mTitle = (TextView) view.findViewById(R.id.todo_title);
            mRoot = view.findViewById(R.id.todo_root);
            mDoneButton = (Button) view.findViewById(R.id.done_button);
        }
    }

    public static class AddTodoViewHolder extends RecyclerView.ViewHolder {
        Button mButton;

        public AddTodoViewHolder(View view) {
            super(view);
            mButton = (Button) view.findViewById(R.id.add_todo);
        }
    }


}
