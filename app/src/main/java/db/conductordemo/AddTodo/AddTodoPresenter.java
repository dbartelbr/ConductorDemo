package db.conductordemo.AddTodo;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import db.conductordemo.R;

/**
 * Created by dbartel on 6/3/16.
 */
public class AddTodoPresenter {

    public interface Callbacks {
        void onAddTodo(String name, String description);
    }

    public static class ViewHolder {
        EditText title;
        EditText description;
        Button add;

        public ViewHolder(View view) {
            title = (EditText) view.findViewById(R.id.title);
            description = (EditText) view.findViewById(R.id.description);
            add = (Button) view.findViewById(R.id.add);
        }
    }

    public static void present(final ViewHolder viewHolder, final Callbacks callbacks) {
        viewHolder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callbacks.onAddTodo(viewHolder.title.getText().toString(), viewHolder.description.getText().toString());
            }
        });
    }
}
