package db.conductordemo.AddTodo;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;

import db.conductordemo.R;
import db.conductordemo.handler.TodoListHandler;
import db.conductordemo.model.TodoItem;

/**
 * Controller which manages adding a new TODO item
 */
public class AddTodoController extends Controller implements  AddTodoPresenter.Callbacks {

    TodoListHandler mHandler;


    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        try {
            mHandler = (TodoListHandler) getActivity();
        } catch (ClassCastException e) {
            throw new ClassCastException("AddTodoController must attach to an activity which implements TodoListHandler");
        }


        View view = inflater.inflate(R.layout.view_add_todo, container, false);
        AddTodoPresenter.ViewHolder viewHolder = new AddTodoPresenter.ViewHolder(view);
        AddTodoPresenter.present(viewHolder, this);
        return view;
    }

    @Override
    public void onAddTodo(String name, String description) {
        // Tell handler we  have a new todo item, then pop the controller since we're done
        mHandler.onAddTodo(new TodoItem(name, description));
        getRouter().popCurrentController();
    }
}
