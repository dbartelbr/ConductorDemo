package db.conductordemo.handler;

import java.util.List;

import db.conductordemo.model.TodoItem;

/**
 * Interface for handling transmission of todo data
 */
public interface TodoListHandler {
    /**
     * When a controller adds an item to the list
     * @param item
     */
    void onAddTodo(TodoItem item);

    /**
     * When a controller removes an item from the list
     * @param item
     */
    void onRemoveTodo(TodoItem item);

    /**
     * When a controller wants the full list
     * @return The full todo list
     */
    List<TodoItem> getTodoList();
}

