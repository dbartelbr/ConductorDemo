package db.conductordemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Conductor;
import com.bluelinelabs.conductor.Router;

import java.util.ArrayList;
import java.util.List;

import db.conductordemo.TodoList.TodoListController;
import db.conductordemo.handler.TodoListHandler;
import db.conductordemo.model.TodoItem;

public class MainActivity extends AppCompatActivity implements TodoListHandler {

    ViewGroup mViewRoot;

    public List<TodoItem> getmTodoItems() {
        return mTodoItems;
    }

    List<TodoItem> mTodoItems;

    private Router mRouter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewRoot = (ViewGroup) findViewById(R.id.view_root);
        mTodoItems = new ArrayList<>();

        // Attach Router & set root
        mRouter = Conductor.attachRouter(this, mViewRoot, savedInstanceState);
        if (!mRouter.hasRootController()) {
            mRouter.setRoot(new TodoListController());
        }
    }

    @Override
    public void onBackPressed() {
        if (!mRouter.handleBack()) {
            super.onBackPressed();
        }
    }


    @Override
    public void onAddTodo(TodoItem item) {
        mTodoItems.add(item);
    }

    @Override
    public void onRemoveTodo(TodoItem item) {
        mTodoItems.remove(item);

    }

    @Override
    public List<TodoItem> getTodoList() {
        return mTodoItems;
    }
}
