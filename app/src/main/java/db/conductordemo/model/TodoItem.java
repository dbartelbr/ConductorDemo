package db.conductordemo.model;

/**
 * Simple Todo Item
 */
public class TodoItem {
    private String mTitle;
    private String mDescription;

    public TodoItem(String title, String description) {
        mTitle = title;
        mDescription = description;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String description) {
        mDescription = description;
    }
}
