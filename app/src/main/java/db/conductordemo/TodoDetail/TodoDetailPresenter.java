package db.conductordemo.TodoDetail;

import android.view.View;
import android.widget.TextView;

import db.conductordemo.AddTodo.AddTodoPresenter;
import db.conductordemo.R;
import db.conductordemo.model.TodoItem;

/**
 * Created by dbartel on 6/3/16.
 */
public class TodoDetailPresenter {
    public static class ViewHolder {
        TextView title;
        TextView description;

        public ViewHolder(View view) {
            title = (TextView) view.findViewById(R.id.todo_title);
            description = (TextView) view.findViewById(R.id.description);
        }
    }

    public static void present(ViewHolder viewHolder, TodoItem todoItem) {
        viewHolder.title.setText(todoItem.getTitle());
        viewHolder.description.setText(todoItem.getDescription());
    }

}
