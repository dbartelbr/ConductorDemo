package db.conductordemo.TodoDetail;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bluelinelabs.conductor.Controller;

import db.conductordemo.R;
import db.conductordemo.model.TodoItem;

/**
 * Created by dbartel on 6/3/16.
 */
public class TodoDetailController extends Controller {

    TodoItem mTodoItem;

    public TodoDetailController() {

    }

    public TodoDetailController(TodoItem item) {
        mTodoItem = item;
    }


    @NonNull
    @Override
    protected View onCreateView(@NonNull LayoutInflater inflater, @NonNull ViewGroup container) {
        View view = inflater.inflate(R.layout.view_todo_detail, container, false);
        TodoDetailPresenter.present(new TodoDetailPresenter.ViewHolder(view), mTodoItem);
        return view;
    }
}
